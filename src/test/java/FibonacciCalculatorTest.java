import org.junit.Assert;
import org.junit.Test;

public class FibonacciCalculatorTest {
    @Test
    public void shouldWorkForBiggerNumbers() {
        Assert.assertEquals(2, FibonacciCalculator.getFibonacciNumber(3));
        Assert.assertEquals(3, FibonacciCalculator.getFibonacciNumber(4));
        Assert.assertEquals(5, FibonacciCalculator.getFibonacciNumber(5));
        Assert.assertEquals(8, FibonacciCalculator.getFibonacciNumber(6));
        Assert.assertEquals(13, FibonacciCalculator.getFibonacciNumber(7));
    }

    @Test
    public void shouldReturnOneForOneAndTwo() {
        Assert.assertEquals(1, FibonacciCalculator.getFibonacciNumber(1));
        Assert.assertEquals(1, FibonacciCalculator.getFibonacciNumber(2));
    }

    @Test
    public void shouldReturnZeroForZero() {
        Assert.assertEquals(0, FibonacciCalculator.getFibonacciNumber(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForNegativeArgument() {
        FibonacciCalculator.getFibonacciNumber(-1);
    }

}