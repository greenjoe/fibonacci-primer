public class FibonacciCalculator {
    public static long getFibonacciNumber(int i) {
        if (i < 0) {
            throw new IllegalArgumentException("Fibonacci number index cannot be < 0");
        }
        if (i == 0) {
            return 0;
        } else {
            return 1;
        }
    }
}
